package ru.coderiders.collections2;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class HashTable<K, V> {
    private final int DEFAULT_INITIAL_CAPACITY = 16; // Начальная ёмкость по умолчанию
    private final float LOAD_FACTOR = 0.7f; // Степень расширения при превышении порога

    private int size = (int) (DEFAULT_INITIAL_CAPACITY * LOAD_FACTOR); // Размер хэш-таблицы
    private int currentSize = 0; // текущий размер
    private int capacity = DEFAULT_INITIAL_CAPACITY;

    public List<Pair> storage = new LinkedList<>();

    private class Pair{
        K key;
        V value;

        public Pair(K key, V value){
            this.key = key;
            this.value = value;
        }
    }

    public HashTable() {
        this.resize(this.DEFAULT_INITIAL_CAPACITY);
    }

    private void resize(int newCapacity){
        this.capacity = newCapacity;
        this.size = (int) (this.capacity * LOAD_FACTOR);

        if (this.storage.size() != 0) {
            List<Pair> temp = new LinkedList<>(this.storage);
            this.storage = new LinkedList<>(Collections.nCopies(this.capacity, null));
            this.storage.addAll(0, temp);
        } else {
            this.storage = new LinkedList<>(Collections.nCopies(this.capacity, null));
        }
    }

    private int hash(K key){
        return (key.hashCode() & 0x7fffffff) % this.size;
    }

    public V get(K key){
        int hash = this.hash(key);

        while (this.storage.get(hash) != null){
            if (this.storage.get(hash).key.equals(key)){
                return this.storage.get(hash).value;
            }

            hash++;
        }

        return null;
    }

    public void add(K key, V value){
        if (this.currentSize == this.size) {
            this.resize(this.capacity * 2);
        }

        Pair element = new Pair(key, value);
        int hash = this.hash(key);
        if (this.get(key) == null) {
            while (this.storage.get(hash) != null) {
                hash++;
            }
        }

        this.storage.set(hash, element);
        this.currentSize++;
    }
}
