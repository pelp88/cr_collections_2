package ru.coderiders.collections2;

public class TestLaunch {
    public static void main(String[] args) {
        HashTable<String, String> map1 = new HashTable<>();
        map1.add("Test1", "aaa");
        map1.add("Test2", "bbb");
        System.out.println(map1.get("Test1"));
        System.out.println(map1.get("Test2"));
        map1.add("Test1", "ccc");
        System.out.println(map1.get("Test1"));

        HashTable<String, Double> map2 = new HashTable<>();
        map2.add("Test1", 0.1);
        map2.add("Test2", 0.2);
        System.out.println(map2.get("Test1"));
        System.out.println(map2.get("Test2"));
        map2.add("Test1", 0.3);
        System.out.println(map2.get("Test1"));

        HashTable<Integer, Integer> map3 = new HashTable<>();
        for (int i = 0; i < 100; i++){
            map3.add(i, i);
        }
        for (int i = 0; i < 100; i++){
            System.out.println(map3.get(i));
        }
    }
}
